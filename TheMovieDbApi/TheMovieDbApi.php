<?php
/**
 * TheMovieDbApi
 *
 * A (very) simple library to access The Movie DB data.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2019, Nihilarr
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   TheMovieDbApi
 * @author    Drew Smith
 * @copyright copyright (c) 2019, Nihilarr (https://www.nihilarr.com)
 * @license	  http://opensource.org/licenses/MIT	MIT License
 * @link      https://gitlab.com/nihilarr/the-movie-db-api
 * @version   0.0.3
 */

namespace Nihilarr;

use \Curl\Curl;

/**
 * TheMovieDbApi library
 */
class TheMovieDbApi {
    /**
     * Main cURL handle
     * @var resource
     */
    private $request;

    /**
     * Last response result
     * @var mixed
     */
    private $result;

    /**
     * TMDb API URL
     * @var string
     */
    private $api_url = 'https://api.themoviedb.org';

    /**
     * TMDb API version
     * @var string
     */
    private $api_version = "3";

    /**
     * TMDb API key
     * @var string
     */
    private $api_key = 'none';

    /**
     * Enable/disable adult results
     * @var bool
     */
    private $include_adult = false;

    /**
     * Language identifier
     * @var string
     */
    private $language = 'en-US';

    /**
     * Enable of disable debugging
     * @var bool
     */
    private $debug = false;

    /**
     * Last error message
     * @var string
     */
    private $error;

    /**
     * Last error code
     * @var int
     */
    private $errno;

    /**
     * Constructor
     * @param string $api_key
     */
    public function __construct(string $api_key = null) {
        $this->api_key = is_null($api_key) ? 'x' : $api_key;

        // Initialize new cURL session
        $this->request = new Curl();
        $this->request->setOpt(CURLOPT_SSL_VERIFYPEER, true);
        $this->request->setOpt(CURLOPT_SSL_VERIFYHOST, 2);
        $this->request->setOpt(CURLOPT_CAINFO, __DIR__ . DIRECTORY_SEPARATOR . 'cacert.pem');
        $this->request->setOpt(CURLOPT_CAPATH, __DIR__ . DIRECTORY_SEPARATOR . 'cacert.pem');
        $this->request->setHeaders(array(
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ));

        // Set main request error handler
        $this->request->error(function($request) {
            $this->errno = $request->errorCode;
            $this->error = "[{$this->errno}] {$request->errorMessage} ({$request->url})";

            if($this->debug) {
                $this->debug_message($this->error);
            }

            trigger_error($this->error, E_USER_ERROR);
        });
    }

    /**
     * Destructor
     */
    public function __destruct() {
        $this->close();
    }

    /**
     * Search for a movie by title.
     * @param string $title Title of the movie.
     * @param string $year Optional. Year of the movie.
     * @param array $params Optional. Extra parameters.
     * @return array|bool Array of objects result, false on failure.
     */
    public function search_movie(string $title, string $year = null, array $params = array()) {
        $params['query'] = $title;

        if(!is_null($year)) {
            $params['year'] = $year;
        }

        return $this->request('search/movie', $params);
    }

    /**
     * Get movie by the TMDb ID.
     * @param string $id TMDb movie ID.
     * @param array $params Optional. Extra parameters.
     * @return object|bool Result, false on failure.
     */
    public function movie(string $id, array $params = array()) {
        return $this->request("movie/{$id}", $params);
    }

    /**
     * Get a single movie by title and optional year.
     * @param string $title Title of the movie.
     * @param string $year Optional. Year of the movie.
     * @param array $params Optional. Extra parameters.
     * @return object|bool Result, false on failure.
     */
    public function movie_by_title(string $title, string $year = null, array $params = array()) {
        if($movie = $this->search_movie($title, $year, $params)) {
            if($movie->total_results >= 1) {
                return $movie->results[0];
            }
        }

        return false;
    }

    /**
     * Get the TMDb system wide configuration information.
     * @return object Configuration object.
     */
    public function api_configuration() {
        return $this->request('configuration');
    }

    /**
     * Get the list of countries (ISO 3166-1 tags) used throughout TMDb.
     * @return array Array of country objects.
     */
    public function countries() {
        return $this->request('configuration/countries');
    }

    /**
     * Get a list of the jobs and departments we use on TMDb.
     * @return array Array of job objects.
     */
    public function jobs() {
        return $this->request('configuration/jobs');
    }

    /**
     * Get the list of languages (ISO 639-1 tags) used throughout TMDb.
     * @return array Array of language objects.
     */
    public function languages() {
        return $this->request('configuration/languages');
    }

    /**
     * Get a list of the officially supported translations on TMDb.
     * @return array Array of string translations.
     */
    public function primary_translations() {
        return $this->request('configuration/primary_translations');
    }

    /**
     * Get the list of timezones used throughout TMDb.
     * @return array Array of timezone objects.
     */
    public function timezones() {
        return $this->request('configuration/timezones');
    }

    /**
     * Set the API key with $api_key parameter. Returns the current API key
     * if $api_key is omitted.
     * @param string $api_key TMDb API key.
     * @return object|string Self or current API key if $api_key omitted.
     */
    public function api_key(string $api_key = null) {
        if(null === $api_key) {
            return $this->api_key;
        }

        $this->api_key = $api_key;
        return $this;
    }

    /**
     * Set the language with $language parameter. Returns the current language
     * if $language is omitted.
     * @param string $language Language code.
     * @return object|string Self or current language if $language omitted.
     */
    public function language(string $language = null) {
        if(null === $language) {
            return $this->language;
        }

        $this->language = $language;
        return $this;
    }

    /**
     * Toggle status with $include_adult parameter. Returns the current status
     * if $include_adult is omitted.
     * @param bool $include_adult Include adult status.
     * @return object|bool Self or current status if $include_adult omitted.
     */
    public function include_adult(bool $include_adult = null) {
        if(null === $include_adult) {
            return !!$this->include_adult;
        }

        $this->include_adult = $include_adult;
        return $this;
    }

    /**
     * Toggle status with $debug parameter. Returns the current status if $debug
     * is omitted.
     * @param bool $debug Debug status.
     * @return object|bool Self or current status if $debug omitted.
     */
    public function debug(bool $debug = null) {
        if(null === $debug) {
            return !!$this->debug;
        }

        $this->debug = $debug;
        return $this;
    }

    /**
     * Return the last error message.
     * @return string Error message.
     */
    public function error() {
        if(isset($this->request->errorMessage)) {
            return $this->request->errorMessage;
        }
    }

    /**
     * Return the last error number.
     * @return int Error number.
     */
    public function errno() {
        if(isset($this->request->errorCode)) {
            return $this->request->errorCode;
        }
    }

    /**
     * Close/destroy main cURL handle.
     * @return void
     */
    public function close() {
        if($this->debug) {
            $this->debug_message('Closing TheMovieDbApi connections.');
        }

        $this->request->close();
    }

    /**
     * Return the last request result.
     * @return object Last response result.
     */
    public function result() {
        return $this->result;
    }

    /**
     * Send GET request to TMDb.
     * @param string $route Route of the request.
     * @param array $params Optional. Extra parameters.
     * @return mixed Array of or object result, false on failure
     */
    private function request(string $route, array $params = array()) {
        $request_uri = $this->build_request_uri($route, $params);

        if($this->debug) {
            $this->debug_message("Sending {$route} request to {$request_uri}");
        }

        $this->request->success(function($request) {
            if($this->debug) {
                $this->debug_message('Request completed successfully');
            }

            return $this->result = $request->response;
        });

        // Do GET request to TMDb
        return $this->request->get($request_uri);
    }

    /**
     * Build TMDb request URI.
     * @param string $route Path of the request.
     * @param array $params Parameters for query string.
     * @return string Request URI.
     */
    private function build_request_uri(string $route, array $params = array()) {
        $params += array(
            'api_key' => $this->api_key,
            'language' => $this->language,
            'include_adult' => $this->include_adult
        );

        $query = http_build_query($params, '', '&', PHP_QUERY_RFC3986);

        $request_uri = "{$this->api_url}/{$this->api_version}/{$route}?{$query}";

        return $request_uri;
    }

    /**
     * Output debug message to console.
     * @param string $message Message to output.
     * @return void
     */
    private function debug_message(string $message) {
        $timestamp = date('Y-m-d H:i:s');
        echo "[{$timestamp}] > {$message}\n";
    }
}
