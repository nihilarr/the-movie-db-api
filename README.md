
# the-movie-db-api

> A (very) simple library to access The Movie DB data

## Requirements

* [PHP](https://secure.php.net/manual/en/install.php) >= 5.3.29
* [php-curl-class](https://github.com/php-curl-class/php-curl-class) >= 8.0

## Install

### Composer
```bash
composer require nihilarr/the-movie-db-api
```

## Usage

```php
require __DIR__ . '/vendor/autoload.php';

use Nihilarr\TheMovieDbApi;

$tmdb = new TheMovieDbApi('api_key');
```

```php
$movie = $tmdb->movie(13310);
var_dump($movie);
```

```php
$movie = $tmdb->movie_by_title('let the right one in', '2008');
var_dump($movie);
```

```php
$movie = $tmdb->search_movie('let the right one in');
var_dump($movie);
```

### Note

## Contributing

## License

MIT © [Drew Smith](https://www.nihilarr.com)
